#! /bin/bash
# A simple shell script to show all the information I want to check before commiting to a project VCS

# First, check if in root directory of git repository and fail otherwise
if [[ ! -d .git ]]; then
    echo "Not at the root of a git repository, aborting."
    exit 1
fi


# Printing git status, it has a lot of useful information about file tracking
echo "############# git status ###############"
git status




printf "\n\n\n############# BlackBox locking #############\n"
# If in blackbox-enabled repository, check for files that might accidentally have been left open
if [[ -d keyrings/live ]]; then
    echo "In a BlackBox enabled repository; checking for for open secure files."
    bballgood=true
    for bbfile in $(blackbox_list_files)
        do if [ -f bbfile ]; then
            echo $bbfile is not locked.
            bballgood=false
        fi
    done
    if [ bballgood ]; then
        echo "All BlackBox files locked."
    fi

else
    echo "Not in a BlackBox-enabled repo."
fi

printf "\n\n\n########## Checking TODOs ###########\n"
rg TODO
if [ $? -ne 0 ]; then
    echo "No TODOs found."
fi

